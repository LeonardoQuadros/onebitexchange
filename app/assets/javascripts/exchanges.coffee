$(document).ready ->

    $('.form-control').change ->
        $('#amount').trigger 'keyup'

    $('#amount').keyup ->
        $.ajax '/convert',
            type: 'GET'
            dataType: 'json'
            data:   {
                        source_currency: $("#source_currency").val(),
                        target_currency: $("#target_currency").val(),
                        amount: $("#amount").val()
                    }
            error: (jqXHR, textStatus, errorThrown) ->
                alert textStatus
            success: (data, text, jqXHR) ->
                $('#result').val(data.value)
        return false;

    $('#change').click ->
        source_currency = $('#source_currency').val()
        target_currency = $('#target_currency').val()
        $('#target_currency').val source_currency
        $('#source_currency').val target_currency
        $('#amount').trigger 'keyup'