require 'rest-client'
require 'json'

class ExchangeService
    def initialize(source_currency, target_currency, amount)
        @source_currency = source_currency
        @target_currency = target_currency
        @amount = amount.to_f
    end
    
    
    def perform
        begin
            exchange_api_url = Rails.application.credentials[Rails.env.to_sym][:currency_api_url]
            exchange_api_key = Rails.application.credentials[Rails.env.to_sym][:currency_api_key]
            if @source_currency == "BTC" or @target_currency == "BTC"
                if @target_currency == "BTC" and @source_currency == "BTC"
                    return @amount
                elsif @target_currency == "BTC" 
                    url = "https://blockchain.info/tobtc?currency=#{@source_currency}&value=#{@amount.to_i}"
                    res = RestClient.get url
                    return "#{JSON.parse(res.body)}".to_f
                else
                    res = RestClient.get "https://api.blockchain.info/stats"
                    btc_dolar = "#{JSON.parse(res.body)['market_price_usd']}".to_f
                    return btc_dolar*@amount if @target_currency == "USD"
                    url = "#{exchange_api_url}?token=#{exchange_api_key}&currency=USD/#{@target_currency}"
                    res = RestClient.get url
                    valor =  JSON.parse(res.body)['currency'][0]['value'].to_f 
                    return (valor * btc_dolar * @amount)
                end
            else
                url = "#{exchange_api_url}?token=#{exchange_api_key}&currency=#{@source_currency}/#{@target_currency}"
                res = RestClient.get url
                value = JSON.parse(res.body)['currency'][0]['value'].to_f
                return (value * @amount)
                
            end
        rescue RestClient::ExceptionWithResponse => e
            e.response
        end
    end
end